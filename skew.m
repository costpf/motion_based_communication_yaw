% ===================================
% FUNCTION X = skew(x)
% -----------
% DESCRIPTION: Generate the skew-symmetric form from a vector in R^3.
% INPUT:    x   Vector in R^3;
% OUTPUT:   X   Skew-symmetric matrix generated from x.        
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function X = skew(x)
X = [0 -x(3) x(2);
     x(3) 0 -x(1);
    -x(2) x(1) 0];
end

