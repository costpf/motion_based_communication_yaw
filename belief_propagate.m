% ===================================
% FUNCTION [r_bar, Sigma_bar, F, Gr, Gx, H, K, Sigma] = belief_propagate(r,Sigma,u,Codebook,numpoints,true_class)
% -----------
% DESCRIPTION:  Propagate belief according to the MHEKF model.
% INPUT:    r         Receiver's state (R^6).
%           Sigma     6-by-6 Covariance matrix of the receiver's state.
%           u         Control input (R^4).
%           Codebbok  Trajectory codebook.
%           numpoints Number of points in one trajectory.
%           true_class True trajectory class.
% OUTPUT:   r_bar     Propagated receiver's state.
%           Sigma_bar Propagated covariance matrix.
%           F         6-by-6 Jacobian of the state transition model.
%           Gr        2n-by-6 Jacobian of the observation model (w.r.t. r).
%           Gx        2n-by-3n Jacobian of the observation model (w.r.t. x).
%           H         2n-by-2n Covariance of the observation likelihood.
%           K         6-by-2n Kalman gain matrix.
%           Sigma     Updated covariance matrix.
% -----------
% DEPENDENCIES: noise_param.mat, F_dr(...), state_trans(...),
%               G_dr_concat(...), G_dx_concat(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function [r_bar, Sigma_bar, F, Gr, Gx, H, K, Sigma] = belief_propagate(r,Sigma,u,Codebook,numpoints,true_class)
% Noise Parameters.
load('noise_param.mat');
F = F_dr(r, u); % Jacobian for r evaluated at the previous state.
% State transition.
r_bar = state_trans(r,u);
Sigma_bar = F*Sigma*F' + Q;
Sigma_bar = (Sigma_bar + Sigma_bar')/2; % To make sure it's symmetric.

Gr = G_dr_concat(r_bar,Codebook{true_class},numpoints);
Gx = G_dx_concat(r_bar,Codebook{true_class},numpoints);
H = Gr*Sigma_bar*Gr' + Gx*S*Gx'+ R;
H = (H + H')/2; % To make sure it's symmetric.
K = Sigma_bar*Gr'/H;

Sigma = (eye(6) - K*Gr)*Sigma_bar;
Sigma = (Sigma + Sigma')/2; % To make sure it's symmetric.
end