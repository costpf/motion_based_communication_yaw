% ===================================
% FUNCTION [] = playback_sim_ellipses(filename)
% -----------
% DESCRIPTION:  Playback the belief state transitions and visualized
%               simulation.
% INPUT:    filename    Filename of the generated simulation file.
% -----------
% DEPENDENCIES: code_define_ellipses(...), visualize(...), retrieve(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function playback_sim_ellipses(filename)
load(filename);
Codebook = code_define_ellipses(numpoints);

true_r = arr_true_r(:,1);
true_x = arr_true_x(:,1);
visible_flags = arr_visible_flags(:,1);
z = arr_z(:,1);
log_phi = arr_log_phi(:,1);
Sigma = arr_Sigma{1};
mu = arr_mu{1};

fprintf('Step: 1\n');
if prod(visible_flags) == 0
    disp('Sender Not Fully Observed.');
end
[~,likely] = max(log_phi);
visualize(true_r, true_x, mu{likely}(1:6), Codebook{likely}, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w);
disp('Log of phi:');
disp(log_phi');
mixand_entropy = zeros(1,length(Codebook));
for ii = 1:length(Codebook)
    mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
end
disp('Class entropy:');
disp(-dot(retrieve(log_phi), log_phi));
disp('Mixand entropies:');
disp(mixand_entropy);
fprintf('\n');

for timestep = 2:length(arr_time)
    true_r = arr_true_r(:,timestep);
    true_x = arr_true_x(:,timestep);
    visible_flags = arr_visible_flags(:,timestep);
    z = arr_z(:,timestep);
    log_phi = arr_log_phi(:,timestep);
    Sigma = arr_Sigma{timestep};
    mu = arr_mu{timestep};
    
    fprintf('Step: %d\n', timestep);
    u = arr_u(:,timestep-1);
    disp('Control Input:');
    disp([u(1:3)', u(4)*180/pi]);
    if prod(visible_flags) == 0
        disp('Sender Not Fully Observed.');
        break;
    end
    disp('Log of phi:');
    disp(log_phi');
    mixand_entropy = zeros(1,length(Codebook));
    for ii = 1:length(Codebook)
        mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
    end
    disp('Class entropy:');
    disp(-dot(retrieve(log_phi), log_phi));
    disp('Mixand entropies:');
    disp(mixand_entropy);
    [~,maxindex] = max(log_phi);
    %maxindex = 1;
    visualize(true_r, true_x, mu{maxindex}(1:6), Codebook{maxindex}, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w);
    fprintf('\n');
end
end