% ===================================
% FUNCTION Gx = G_dx_concat(r,x,numpoints)
% -----------
% DESCRIPTION:  Compute the concatenated Jacobian matrix (derivatives with
%               respect to x).
% INPUT:    r   Receiver's state (R^6).
%           x   Trajectory (R^(3*n)).
%           numpoints   Number of points in one trajectory.
% OUTPUT:   Gx  2n-by-3n Jacobian matrix (sparese).
% DEPENDENCIES: G_dx(...)   
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function Gx = G_dx_concat(r,x,numpoints)
Gx = sparse(2*numpoints, 3*numpoints);
x = reshape(x,3,numpoints);
for ii = 1:numpoints
    Gx(2*ii-1:2*ii,3*ii-2:3*ii) = G_dx(r,x(:,ii));
end
end