% ===================================
% FUNCTION [true_r, true_x, mu, Sigma, log_phi, z, visible_flag, visible_flags] = ekf_update(true_r, mu, Sigma, log_phi, u, Codebook, numpoints, true_class)
% -----------
% DESCRIPTION:  Perform one-step MHEKF update for the belief state.
% INPUT:    true_r  true receiver's state (R^6).
%           mu      Cellarray of state means.
%           Sigma   Cellarray of state covariances.
%           log_phi Log of multinomial parameters.
%           u       Control input (R^4).
%           Codebook Trajectory codebook.
%           numpoints   Number of points in one trajectory.
%           true_class  True trajectory class.
% OUTPUT:   true_r  updated true receiver's state (R^6).
%           true_x  true trajectory (R^(3*n)).
%           mu      Cellarray of state means.
%           Sigma   Cellarray of state covariances.
%           log_phi Log of multinomial parameters.
%           z       Observation taken (R^(2*n)).
%           visible_flag    1 if all the points in the trajectory observed.
%           visible_flags   1 if corresponding point in the trajectory
%                           observed.
% -----------
% DEPENDENCIES: noise_param.mat, state_trans(...), observe_actual(...),
%               belief_propagate(...), observe(...), logdet(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function [true_r, true_x, mu, Sigma, log_phi, z, visible_flag, visible_flags] = ekf_update(true_r, mu, Sigma, log_phi, u, Codebook, numpoints, true_class)
load('noise_param.mat');

% True State Transition
true_r = state_trans(true_r, u);
true_r = true_r + transpose(mvnrnd([0;0;0;0;0;0],Q));
true_x = Codebook{true_class} + transpose(mvnrnd(sparse(3*numpoints,1),full(S)));

% Take a new noisy observation.
[z,visible_flag,visible_flags] = observe_actual(true_r, true_x);

% MHEKF update.
for ii = 1:length(log_phi)
    % State transition estimation
    [r_bar,~,~,~,~,H,K,Sigma{ii}] = belief_propagate(mu{ii},Sigma{ii},u,Codebook,numpoints,ii);
    mu_bar = r_bar;
    z_est = observe(r_bar, Codebook{ii});
    innov = z - z_est;
    mu{ii} = mu_bar + K*innov;
    % Avoid singularities
    if norm(mu{ii}(1:3)) > pi;
        mu{ii}(1:3) = (1 - 2*pi/norm(mu{ii}(1:3)))*mu{ii}(1:3);
    end
    log_phi(ii) = -1/2*logdet(2*pi*H) - 1/2*innov'*(H\innov) + log_phi(ii);
end
log_phi = log_phi - repmat(max(log_phi),length(Codebook),1);

% Prevent phi to become inaccurate due to computational limits.
for ii = 1:length(log_phi)
    if log_phi(ii) < -7.45e2
        log_phi(ii) = -7.45e2; %exp(-7.46e2) == 0, exp(-7.45e2) = 4.9407e-324
    end
end
end