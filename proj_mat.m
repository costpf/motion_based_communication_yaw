% ===================================
% FUNCTION P = proj_mat(r)
% -----------
% DESCRIPTION: Generate the camera projection matrix from the parameters.
% INPUT:    r   current state of the receiver.
% OUTPUT:   P   4-by-3 Camera Projection Matrix.
% -----------
% DEPENDENCIES: skew(x), camera_param.mat
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function P = proj_mat(r)
load('camera_param.mat');
w = [r(1);r(2);r(3)];
t = [r(4);r(5);r(6)];
R = expm(skew(w))';
t = -R*t - [d_bc;0;0];
P = K*R_bc*[R,t]; % 4x3 Projection Matrix.
end