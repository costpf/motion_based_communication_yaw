clear;clc;close all;

numpoints = 20;
numsteps = 100;
numtrials = 150;

Codebook = code_define_ellipses(numpoints);
accurate = zeros(numtrials,numsteps,2);
entropies_class = zeros(numtrials,numsteps,2);
entropies_state = zeros(numtrials,numsteps,2);
error_r = zeros(numtrials,numsteps,2);
timesteps = zeros(numtrials,2);

noise_define(numpoints);
load('noise_param.mat', 'S');

for trial = 1:numtrials
    
    true_class = randi(length(Codebook));
    
    temp_true_Rot_traj2w = zeros(3,3);
    temp_true_trans_traj2w = zeros(3,1);
    temp_true_r = zeros(6,1);
    temp_true_x = zeros(3*numpoints,1);
    temp_mu = cell(1,1);
    temp_Sigma = cell(1,1);
    temp_z = zeros(2*numpoints,1);
    temp_log_phi = zeros(length(Codebook),1);
    temp_visible_flags = zeros(numpoints,1);
    
    for policy = 1:2
        
        fprintf('Policy: %d, Trial: %d\n', policy, trial);
        filename = sprintf('sim_results_policy_%d_trial_%.3d_class_%d.mat', policy, trial, true_class);
        
        arr_true_r = zeros(6,numsteps);
        arr_true_x = zeros(3*numpoints,numsteps);
        arr_mu = cell(1,numsteps);
        arr_Sigma = cell(1,numsteps);
        arr_log_phi = zeros(length(Codebook),numsteps);
        arr_class_entropy = zeros(1,numsteps);
        arr_state_entropy = zeros(1,numsteps);
        arr_z = zeros(2*numpoints,numsteps);
        arr_visible_flags = zeros(numpoints,numsteps);
        arr_time = 1:1:numsteps;
        arr_u = zeros(4,numsteps-1);
        
        
        fprintf('Step: 1\n');
        switch policy
            case 1
                % Define trajectory coordinates (in world coordinates).
                %w_traj2w = [0.3;0.9;1.3];
                w_traj2w = [rand();rand();rand()];
                true_Rot_traj2w = expm(skew(w_traj2w));
                true_trans_traj2w = [10;5;5];
                
                % Initial configuration (in trajectory coordinates).
                true_r = [inv_skew(logm(true_Rot_traj2w')); true_Rot_traj2w'*[-10;0;-0.5]];
                
                % Initial trajectory generation.
                true_x = Codebook{true_class} + transpose(mvnrnd(sparse(3*numpoints,1),full(S)));
                
                % Take initial observation and Initialize belief.
                [z,visible_flag,visible_flags] = observe_actual(true_r, true_x);
                if visible_flag == 0
                    disp('Sender Not Fully Observed.');
                end
                [mu, Sigma, log_phi] = prior_rob(z, Codebook);
                [~,likely] = max(log_phi);
                if likely == true_class
                    accurate(trial,1,policy) = 1;
                end
                visualize(true_r, true_x, mu{likely}(1:6), Codebook{likely}, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w);
                disp('Log of phi:');
                disp(log_phi');
                mixand_entropy = zeros(1,length(Codebook));
                for ii = 1:length(Codebook)
                    mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
                end
                state_entropy = dot(retrieve(log_phi), mixand_entropy);
                class_entropy = -dot(retrieve(log_phi), log_phi);
                disp('Class entropy:');
                disp(class_entropy);
                disp('State entropy:');
                disp(state_entropy);
                fprintf('\n');
                temp_true_Rot_traj2w = true_Rot_traj2w;
                temp_true_trans_traj2w = true_trans_traj2w;
                arr_true_r(:,1) = true_r;
                temp_true_r = true_r;
                arr_true_x(:,1) = true_x;
                temp_true_x = true_x;
                arr_mu{1} = mu;
                temp_mu = mu;
                error_r(trial,1,policy) = norm(true_r - mu{likely});
                arr_Sigma{1} = Sigma;
                temp_Sigma = Sigma;
                arr_log_phi(:,1) = log_phi;
                temp_log_phi = log_phi;
                arr_state_entropy(:,1) = state_entropy;
                entropies_state(trial,1,policy) = state_entropy;
                arr_class_entropy(:,1) = class_entropy;
                entropies_class(trial,1,policy) = class_entropy;
                arr_z(:,1) = z;
                temp_z = z;
                arr_visible_flags(:,1) = visible_flags; % This is an assumption.
                temp_visible_flags = visible_flags;
            case 2
                true_Rot_traj2w = temp_true_Rot_traj2w;
                true_trans_traj2w = temp_true_trans_traj2w;
                true_r = temp_true_r;
                true_x(:,1) = temp_true_x;
                mu = temp_mu;
                Sigma = temp_Sigma;
                log_phi = temp_log_phi;
                z = temp_z;
                visible_flags = temp_visible_flags;
                clear temp_true_Rot_traj2w temp_true_trans_traj2w temp_true_r temp_true_x temp_mu temp_Sigma temp_log_phi temp_z temp_visible_flags;
                [~,likely] = max(log_phi);
                if likely == true_class
                    accurate(trial,1,policy) = 1;
                end
                visualize(true_r, true_x, mu{likely}(1:6), Codebook{likely}, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w);
                disp('Log of phi:');
                disp(log_phi');
                mixand_entropy = zeros(1,length(Codebook));
                for ii = 1:length(Codebook)
                    mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
                end
                state_entropy = dot(retrieve(log_phi), mixand_entropy);
                class_entropy = -dot(retrieve(log_phi), log_phi);
                disp('Class entropy:');
                disp(class_entropy);
                disp('State entropy:');
                disp(state_entropy);
                fprintf('\n');
                arr_true_r(:,1) = true_r;
                arr_true_x(:,1) = true_x;
                arr_mu{1} = mu;
                error_r(trial,1,policy) = norm(true_r - mu{likely});
                arr_Sigma{1} = Sigma;
                arr_log_phi(:,1) = log_phi;
                arr_state_entropy(:,1) = state_entropy;
                entropies_state(trial,1,policy) = state_entropy;
                arr_class_entropy(:,1) = class_entropy;
                entropies_class(trial,1,policy) = class_entropy;
                arr_z(:,1) = z;
                arr_visible_flags(:,1) = visible_flags; % This is an assumption.
        end
        
        for timestep = 2:numsteps
            fprintf('Step: %d\n', timestep);
            switch policy
                case 1
                    u = control_random(log_phi, mu, Sigma, Codebook, numpoints);
                case 2
                    u = control_state(log_phi, mu, Sigma, Codebook, numpoints);
            end
            [true_r, true_x, mu, Sigma, log_phi, z, visible_flag, visible_flags] = ekf_update(true_r, mu, Sigma, log_phi, u, Codebook, numpoints, true_class);
            if visible_flag == 0
                disp('Sender Not Fully Observed.');
                break;
            end
            disp('Log of phi:');
            disp(log_phi');
            mixand_entropy = zeros(1,length(Codebook));
            for ii = 1:length(Codebook)
                mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
            end
            state_entropy = dot(retrieve(log_phi), mixand_entropy);
            class_entropy = -dot(retrieve(log_phi), log_phi);
            disp('Class entropy:');
            disp(class_entropy);
            disp('State entropy:');
            disp(state_entropy);
            [~,maxindex] = max(log_phi);
            if maxindex == true_class
                accurate(trial,timestep,policy) = 1;
            end
            visualize(true_r, true_x, mu{maxindex}(1:6), Codebook{maxindex}, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w);
            fprintf('\n');
            
            arr_true_r(:,timestep) = true_r;
            arr_true_x(:,timestep) = true_x;
            arr_mu{timestep} = mu;
            error_r(trial,timestep,policy) = norm(true_r - mu{maxindex});
            arr_Sigma{timestep} = Sigma;
            arr_log_phi(:,timestep) = log_phi;
            arr_class_entropy(timestep) = class_entropy;
            entropies_class(trial,timestep,policy) = class_entropy;
            arr_state_entropy(timestep) = state_entropy;
            entropies_state(trial,timestep,policy) = state_entropy;
            arr_z(:,timestep) = z;
            arr_visible_flags(:,timestep) = visible_flag;
            arr_u(:,timestep-1) = u;
        end
        timesteps(trial,policy) = timestep;
        save(filename, 'arr_true_r', 'arr_true_x', 'arr_mu', 'arr_Sigma', 'arr_log_phi', 'arr_z', 'arr_class_entropy', 'arr_state_entropy', 'arr_visible_flags', 'arr_u', 'arr_time', 'numpoints', 'true_class', 'true_Rot_traj2w', 'true_trans_traj2w');
        save('accurate.mat', 'accurate', 'error_r','entropies_class', 'entropies_state', 'timesteps', 'trial', 'policy');
    end
end