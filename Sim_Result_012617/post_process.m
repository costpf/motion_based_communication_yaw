clear;clc;close all;
load('accurate.mat')
time = max(max(timesteps));
count = 0;
num = 50;
accu_ext_rand = zeros(0,time);
accu_ext_entr = zeros(0,time);
error_r_ext_rand = zeros(0,time);
error_r_ext_entr = zeros(0,time);
for ii = 1:trial
    if (timesteps(ii,1) == time) && (timesteps(ii,2) == time)
        count = count + 1;
        accu_ext_rand(end+1,:) = accurate(ii,:,1);
        accu_ext_entr(end+1,:) = accurate(ii,:,2);
        error_r_ext_rand(end+1,:) = error_r(ii,:,1);
        error_r_ext_entr(end+1,:) = error_r(ii,:,2);
    end
    if count == num
        break;
    end
end
figure(1);
plot(1:1:time, mean(accu_ext_rand).*100,'LineWidth',2,'LineStyle','--'); grid on; hold on;
plot(1:1:time, mean(accu_ext_entr).*100,'LineWidth',2);
legend('Random Policy', 'Entropy Policy','Location','SouthEast');
ylim([0,100]);
xlim([1,100]);
ylabel('Classification Accuracy');
xlabel('Timesteps');

figure(2)
plot(1:1:time, mean(error_r_ext_rand),'LineWidth',2,'LineStyle','--'); grid on; hold on;
plot(1:1:time, mean(error_r_ext_entr),'LineWidth',2);
legend('Random Policy', 'Entropy Policy','Location','NorthEast');
xlim([1,100]);
ylabel('Pose Estimation Error');
xlabel('Timesteps');