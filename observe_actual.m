% ===================================
% FUNCTION [z, visible_flag, visible_flags] = observe_actual(r,x)
% -----------
% DESCRIPTION:  Take an actual observation with noise.
% INPUT:    r   Receiver's state (R^6).
%           x   Trajectory (R^(3*n)).
% OUTPUT:   z   Observation (R^(2*n)) 
% DEPENDENCIES: camera_param.mat, noise_param.mat, proj_mat(r), 
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function [z,visible_flag,visible_flags] = observe_actual(r,x)
numpoints = length(x)/3;
load('camera_param.mat'); load('noise_param.mat');
U0 = K(1,3);
V0 = K(2,3);

P = proj_mat(r);
x = reshape(x,3,numpoints);
z = zeros(2,numpoints);
visible_flags = ones(1,numpoints);
for ii = 1:numpoints
    proj = P*[x(:,ii);1];
    u = proj(1)/proj(3);
    v = proj(2)/proj(3);
    z(:,ii) = [u;v];
    if proj < 0;
        visible_flags(ii) = 0;
    end
end
z = reshape(z,2*numpoints,1) + transpose(mvnrnd(zeros(2*numpoints,1),full(R)));

z = reshape(z,2,numpoints);
for ii = 1:numpoints
    u = z(1,ii);
    v = z(2,ii);
    if ((u < 0 || u > 2*U0) || (v < 0 || v > 2*V0))
        visible_flags(ii) = 0;
    end
end

z = reshape(z,2*numpoints,1);
visible_flag = prod(visible_flags);
end