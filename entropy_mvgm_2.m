function H2 = entropy_mvgm_2(phi,mu,Sigma)
    function J = mvgm_d1(x,gmobj)
        weights = -gmobj.ComponentProportion'.*mvnpdf(x',gmobj.mu,gmobj.Sigma);
        compJ = zeros(length(x),gmobj.NumComponents);
        for jj = 1:gmobj.NumComponents
            compJ(:,jj) = weights(jj)*(gmobj.Sigma(:,:,jj)\(x - gmobj.mu(jj,:)'));
        end
        J = sum(compJ,2);
    end
    function F = mvgm_d2(x,gmobj)
        gmpd_x = pdf(gmobj,x');
        if gmpd_x == 0;
            F = 0;
        else
            weights = gmobj.ComponentProportion'.*mvnpdf(x',gmobj.mu,gmobj.Sigma);
            compF = zeros(length(x),length(x),gmobj.NumComponents);
            for jj = 1:gmobj.NumComponents
                compF(:,:,jj) = weights(jj)*((gmobj.Sigma(:,:,jj)\(x - gmobj.mu(jj,:)'))*transpose(mvgm_d1(x,gmobj))./gmpd_x...
                    + (gmobj.Sigma(:,:,jj)\((x - gmobj.mu(jj,:)')*((x - gmobj.mu(jj,:)')'/gmobj.Sigma(:,:,jj)) - eye(length(x)))));
                %compF(:,:,jj) = weights(jj).*(gmobj.Sigma(:,:,jj)\(((x - gmobj.mu(jj,:)')./gmpd_x)*transpose(mvgm_d1(x,gmobj))...
                %    + (x - gmobj.mu(jj,:)')*transpose(gmobj.Sigma(:,:,jj)\((x - gmobj.mu(jj,:)'))) - eye(length(x))));
            end
            F = sum(compF,3)./gmpd_x;
            %F = sum(compF,3);
        end
    end

mu_arr = cat(2,mu{:})';
Sigma_arr = cat(3,Sigma{:});
gmobj = gmdistribution(mu_arr,Sigma_arr,phi');

comp = zeros(length(phi),1);
for ii = 1:length(phi)
    comp(ii) = phi(ii)/2*sum(sum(mvgm_d2(gmobj.mu(ii,:)',gmobj).*gmobj.Sigma(:,:,ii)));
end
H2 = entropy_mvgm_1(phi,mu,Sigma) - sum(comp);
end