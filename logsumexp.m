% ===================================
% FUNCTION s = logsumexp(log_coeffs, xs)
% -----------
% DESCRIPTION:  Compute log(sum_i^n exp(log(lambda_i)*x_i))
% INPUT:    log_coeffs  Coefficients of parameters xs in ln().
%           xs  Parameters.
% OUTPUT:   s   Resulting value.              
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Revised for the major update.
% ===================================
function s = logsumexp(log_coeffs,xs)
y = log_coeffs+xs;
y_max = max(y);
s = y_max + log(sum(exp(y - y_max)));
if isinf(s)
    disp('Overflow or Underflow occured!')
end
end