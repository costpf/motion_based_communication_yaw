% ===================================
% FUNCTION Codebook = code_define_ellipses(numpoints)
% -----------
% DESCRIPTION:  Define the trajectory codebook.
% INPUT:    numpoints Number of points in one trajectory.
% OUTPUT:   Codebbok  Trajectory codebook (Cellarray with 3n-by-1
%                     concatenated vectors.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function Codebook = code_define_ellipses(numpoints)
T = linspace(0, 2*pi, numpoints);
Codebook = cell(4,1);
for ii = 1:4;
    Codebook{ii} = zeros(length(T),3);
end
for ii = 1:length(T);
    Codebook{1}(ii,:) = 1.5*([-sin(T(ii)), 0, cos(T(ii))] - [0,0,1]); %Codebook1: Circular Trajectory
    Codebook{2}(ii,:) = 1.5*([-0.75*sin(T(ii)), 0, cos(T(ii))] - [0,0,1]); % Codebook2: Elliptic Trajectory
    Codebook{3}(ii,:) = 1.5*([-0.5*sin(T(ii)), 0, cos(T(ii))] - [0,0,1]); % Codebook3: Elliptic Trajectory 2
    Codebook{4}(ii,:) = 1.5*([-0.25*sin(T(ii)), 0, cos(T(ii))] - [0,0,1]); % Codebook4: Elliptic Trajectory 3
end
for ii = 1:4;
    Codebook{ii} = reshape(transpose(Codebook{ii}),3*numpoints,1);
end
end