% ===================================
% FUNCTION logdet = logdet(A)
% -----------
% DESCRIPTION:  Calculate log(det(A)) accurately using Cholesky decomposition for
%               positive-definite, symmetric matrix A.
% INPUT:    A   Positive-definite, symmetric matrix A.
% OUTPUT:   logdet   Resulting value.              
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Revised for the major update.
% ===================================
function logdet = logdet(A)
% 
[L,not_positive] = chol(A);
if not_positive ~= 0
    disp('The matrix is not positive definite.');
    logdet = log(det(A));
else
    logdet = 2*sum(log(diag(L)));
end