% ===================================
% FUNCTION x = inv_skew(X)
% -----------
% DESCRIPTION: Retrieve vector x from its skew symmetric form X
% INPUT:    X   Skew-symmetric matrix generated from x.      
% OUTPUT:   x   Vector in R^3.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function x = inv_skew(X)
x = zeros(3,1);
x(1) = 1/2*(X(3,2) - X(2,3));
x(2) = 1/2*(X(1,3) - X(3,1));
x(3) = 1/2*(X(2,1) - X(1,2));
end
