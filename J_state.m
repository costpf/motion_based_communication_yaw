% ===================================
% FUNCTION [J1, J2] = J_state(log_phi, mu, Sigma, u, Codebook, numpoints)
% -----------
% DESCRIPTION:  Compute the cost functions.
% INPUT:    log_phi   Log of the multinomial parameters.
%           mu        Cellarray of state means.
%           Sigma     Cellarray of state covariance matrices.
%           u         New input in R^4.
%           Codebook  Trajectory codebook.
%           numpoints Number of points in one trajectory.
% OUTPUT:   J1   Belief-state entropy cost function.
%           J2   Optical cost function.
% DEPENDENCIES: noise_param.mat, camera_param.mat, retrieve(...),
%               belief_propagate(...), logdet(...), observe(...),
%               logsumexp(...),
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
%   Haruki Nishimura    11-19-16    <-  GM entropy refined to the 2nd order
%                                       approimation.
% ===================================
function [J1, J2] = J_state(log_phi, mu, Sigma, u, Codebook, numpoints)
load('noise_param.mat');load('camera_param.mat');
phi = retrieve(log_phi);

H = cell(length(Codebook),1);
Sig = cell(length(Codebook),1);
Sig_bar = cell(length(Codebook),1);
logdet_2piH = zeros(length(Codebook),1);
logdet_2piSig = zeros(length(Codebook),1);
z_est = cell(length(Codebook),1);
%alpha = zeros(length(Codebook),1);
opt_mean = zeros(2*numpoints,1);

% Check whether H(m) is small enough.
thresh = 1.e-310;
if -dot(phi,log_phi) < thresh
    [~,index_phi_max] = max(phi);
    [r_bar_max,~,~,~,~,H_max,~,Sig_max] = belief_propagate(mu{index_phi_max}(1:6),Sigma{index_phi_max},u,Codebook,numpoints,index_phi_max);
    J1 = 1/2*logdet(2*pi*Sig_max);
    
    U0 = K(1,3);
    V0 = K(2,3);
    opt_mean_max = observe(r_bar_max, Codebook{index_phi_max});
    %J2 = (norm(opt_mean_max)^2 + trace(H_max) - 2*dot(repmat([U0,V0],1,numpoints),opt_mean_max) + dot(repmat([U0,V0],1,numpoints), repmat([U0,V0],1,numpoints)))/numpoints;
    J2 = norm(opt_mean_max - repmat([U0;V0],numpoints,1))^2/numpoints;
else
    for ii = 1:length(Codebook)
        r = mu{ii}(1:6);
        [r_bar,Sig_bar{ii},~,~,~,H{ii},~,Sig{ii}] = belief_propagate(r,Sigma{ii},u,Codebook,numpoints,ii);
        logdet_2piH(ii) = logdet(2*pi*H{ii});
        logdet_2piSig(ii) = logdet(2*pi*Sig{ii});
        z_est{ii} = observe(r_bar,Codebook{ii});
        
        opt_mean = opt_mean + phi(ii)*z_est{ii};
    end
    
%     % Compute (1st-order-approximated) gaussian mixture entropy
%     for ii = 1:length(Codebook)
%         exponents = zeros(length(Codebook),1);
%         for jj = 1:length(Codebook)
%             exponents(jj) = -1/2*(z_est{ii} - z_est{jj})'*(H{jj}\(z_est{ii} - z_est{jj}));
%         end
%         alpha(ii) = logsumexp(log_phi,exponents);
%     end
    % Compute (2nd-order-approximated) gaussian mixture entropy
    
    Entropy_gm = entropy_mvgm_2(phi, z_est, H);
    
    
    % Compute the Joint-belief-state entropy (only u dependent terms.)
    J1 = dot(phi, (1/2.*logdet_2piH)) + dot(phi, (1/2.*logdet_2piSig)) - Entropy_gm;
    
    % Optical cost function.
    U0 = K(1,3);
    V0 = K(2,3);
    
    opt_var = zeros(2*numpoints,2*numpoints);
    for ii = 1:length(Codebook)
        opt_var = opt_var + phi(ii)*H{ii} + phi(ii)*(z_est{ii} - opt_mean)*(z_est{ii} - opt_mean)';
    end
    %J2 = (norm(opt_mean)^2 + trace(opt_var) - 2*dot(repmat([U0,V0],1,numpoints),opt_mean) + dot(repmat([U0,V0],1,numpoints), repmat([U0,V0],1,numpoints)))/numpoints;
    J2 = norm(opt_mean - repmat([U0;V0],numpoints,1))^2/numpoints;
end

if isinf(J1);
    disp('J is -Inf!!');
end
if isnan(J1);
    disp('J is NaN!!');
end