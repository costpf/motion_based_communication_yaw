clear;clc;
close all;
load('Codebook.mat');
for code = 1:length(Codebook);
    trajectory = transpose(reshape(transpose(Codebook{code}), 3, length(Codebook{code})/3));
    figure(code);
    patch([-1,-1,1,1],[1,-1,-1,1],'w');
    hold on;
    scatter(trajectory(:,1), trajectory(:,2),80,'b','filled');
    axis equal;
    xlabel('x');
    ylabel('y');
    title(sprintf('Trajectory Class %d', code));
end