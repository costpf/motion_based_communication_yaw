clear;clc;close all;

numpoints = 20;
numsteps = 50;
Codebook = code_define_ellipses(numpoints);
true_class = 3;
%true_class = 1;

noise_define(numpoints);
load('noise_param.mat', 'S');

arr_true_r = zeros(6,numsteps);
arr_true_x = zeros(3*numpoints,numsteps);
arr_mu = cell(1,numsteps);
arr_Sigma = cell(1,numsteps);
arr_log_phi = zeros(length(Codebook),numsteps);
arr_class_entropy = zeros(1,numsteps);
arr_state_entropy = zeros(1,numsteps);
arr_z = zeros(2*numpoints,numsteps);
arr_visible_flags = zeros(numpoints,numsteps);
arr_time = 1:1:numsteps;
arr_u = zeros(4,numsteps-1);


fprintf('Step: 1\n');

% Define trajectory coordinates (in world coordinates).
w_traj2w = [0;0;pi/6];
%w_traj2w = [0;0;pi/2];
true_Rot_traj2w = expm(skew(w_traj2w));
true_trans_traj2w = [10;5;5];

% Initial configuration (in trajectory coordinates).
true_r = [inv_skew(logm(true_Rot_traj2w')); true_Rot_traj2w'*[-8;0;-1.5]];

% Initial trajectory generation.
true_x = Codebook{true_class} + transpose(mvnrnd(sparse(3*numpoints,1),full(S)));

% Take initial observation and Initialize belief.
[z,visible_flag,visible_flags] = observe_actual(true_r, true_x);
if visible_flag == 0
    disp('Sender Not Fully Observed.');
end
[mu, Sigma, log_phi] = prior_rob(z, Codebook);
[~,likely] = max(log_phi);
while likely == true_class
    [mu, Sigma, log_phi] = prior_rob(z, Codebook);
    [~,likely] = max(log_phi);
end
visualize(true_r, true_x, mu{likely}(1:6), Codebook{likely}, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w);
disp('Log of phi:');
disp(log_phi');
mixand_entropy = zeros(1,length(Codebook));
for ii = 1:length(Codebook)
     mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
end
state_entropy = dot(retrieve(log_phi), mixand_entropy);
class_entropy = -dot(retrieve(log_phi), log_phi);
disp('Class entropy:');
disp(class_entropy);
disp('State entropy:');
disp(state_entropy);
fprintf('\n');
arr_true_r(:,1) = true_r;
arr_true_x(:,1) = true_x;
arr_mu{1} = mu;
arr_Sigma{1} = Sigma;
arr_log_phi(:,1) = log_phi;
arr_state_entropy(:,1) = state_entropy;
arr_class_entropy(:,1) = class_entropy;
arr_z(:,1) = z;
arr_visible_flags(:,1) = visible_flags; % This is an assumption.

for timestep = 2:numsteps
    fprintf('Step: %d\n', timestep);
    u = control_state(log_phi, mu, Sigma, Codebook, numpoints);
    %u = control_naive(log_phi, mu, Sigma, Codebook, numpoints);
    %u = control_random(log_phi, mu, Sigma, Codebook, numpoints);
    [true_r, true_x, mu, Sigma, log_phi, z, visible_flag, visible_flags] = ekf_update(true_r, mu, Sigma, log_phi, u, Codebook, numpoints, true_class);
    if visible_flag == 0
        disp('Sender Not Fully Observed.');
        break;
    end
    disp('Log of phi:');
    disp(log_phi');
    mixand_entropy = zeros(1,length(Codebook));
    for ii = 1:length(Codebook)
        mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
    end
    state_entropy = dot(retrieve(log_phi), mixand_entropy);
    class_entropy = -dot(retrieve(log_phi), log_phi);
    disp('Class entropy:');
    disp(class_entropy);
    disp('State entropy:');
    disp(state_entropy);
    [~,maxindex] = max(log_phi);
    %maxindex = 1;
    visualize(true_r, true_x, mu{maxindex}(1:6), Codebook{maxindex}, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w);
    fprintf('\n');
    
    arr_true_r(:,timestep) = true_r;
    arr_true_x(:,timestep) = true_x;
    arr_mu{timestep} = mu;
    arr_Sigma{timestep} = Sigma;
    arr_log_phi(:,timestep) = log_phi;
    arr_class_entropy(timestep) = class_entropy;
    arr_state_entropy(timestep) = state_entropy;
    arr_z(:,timestep) = z;
    arr_visible_flags(:,timestep) = visible_flag;
    arr_u(:,timestep-1) = u;
end
save('sim_results.mat', 'arr_true_r', 'arr_true_x', 'arr_mu', 'arr_Sigma', 'arr_log_phi', 'arr_z', 'arr_class_entropy', 'arr_state_entropy', 'arr_visible_flags', 'arr_u', 'arr_time', 'numpoints', 'true_class', 'true_Rot_traj2w', 'true_trans_traj2w');