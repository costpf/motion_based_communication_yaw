function code_define()

numpoints = 20; % Number of points in one trajectory.

global Codebook h points point_index code_index;
Codebook = {}; % Define a cell array to store Codebook.
points = zeros(1,2); % Define an array to store coordinates of points on trajectory.
point_index = 1; code_index = 1;

figure('WindowButtonDownFcn',@code_record_start,'WindowButtonUpFcn',@code_record_end);
axis([-1,1,-1,1]);
xlabel('x'), ylabel('y');
title('Draw a new trajectory below.');
patch([-1,-1,1,1],[1,-1,-1,1],'w'); % Drawing area
hold on;
axis equal;

    function code_record_start(src,eventdata)
    % While the mouse button is pressed.
    % Start the recording.
        set(src,'WindowButtonMotionFcn', @record);
        function record(src,eventdata)
        % While the mouse cursor is moving.
            cur_point = get(gca,'CurrentPoint'); % Extract current cursor coordinates.
            x = cur_point(1,1);
            y = cur_point(1,2);
                points(point_index,:) = [x,y];
                point_index = point_index + 1;
            t = linspace(0,2*pi,100);
            pos_x_draw = 0.01*sin(t)' + repmat(x,size(sin(t),1));
            pos_y_draw = 0.01*cos(t)' + repmat(y,size(sin(t),1));
            h(point_index) = patch(pos_x_draw,pos_y_draw,'k'); % Draw the new point.
            drawnow;
        end
    end

    function code_record_end(src, eventdata)
    % When the mouse button is released.
        set(src,'WindowButtonMotionFcn',''); % Stop recording.
        % Resample points using numpoints.
        x_resampled = interp1((0:1:length(points)-1), points(:,1), (0:1:numpoints-1).*(length(points)-1)/numpoints);
        y_resampled = interp1((0:1:length(points)-1), points(:,2), (0:1:numpoints-1).*(length(points)-1)/numpoints);
        % Shift trajectory to fit in the square.
        x_resampled = x_resampled - (min(x_resampled) + range(x_resampled)/2);
        y_resampled = y_resampled - (min(y_resampled) + range(y_resampled)/2);
        % Draw the resulting trajectory.
        figure(2);
        patch([-1,-1,1,1],[1,-1,-1,1],'w');
        hold on;
        axis equal;
        scatter(x_resampled,y_resampled);
        xlabel('x');
        ylabel('y');
        title('Resulting Trajectory')
        % Ask the user if the trajectory should be saved or not.
        while true;
            user_resp = input('save the trajectory? (y/n)\n','s');
            if strcmp(user_resp,'y')
                % Register the trajectory to the codebook.
                Codebook{code_index} = [x_resampled', y_resampled', zeros(numpoints,1)];
                Codebook{code_index} = reshape(transpose(Codebook{code_index}),3*numpoints,1);
                fprintf('New trajectory class defined as class %d.\n', code_index);
                code_index = code_index + 1;
                points = zeros(1,2); % Reset points;
                point_index = 1;
                % Reset figures
                figure(1);
                hold off; 
                patch([-1,-1,1,1],[1,-1,-1,1],'w'); % Drawing area
                hold on;
                figure(2);
                close;
                save('Codebook.mat','Codebook');
                break;
            elseif strcmp(user_resp,'n')
                disp('Trajectory discarded.')
                points = zeros(1,2); % Reset points.
                point_index = 1;
                % Reset figures
                figure(1);
                hold off; 
                patch([-1,-1,1,1],[1,-1,-1,1],'w'); % Drawing area
                hold on;
                figure(2);
                close;
                break;
            end
        end
    end
end