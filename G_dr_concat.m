% ===================================
% FUNCTION Gr = G_dr_concat(r,x,numpoints)
% -----------
% DESCRIPTION:  Compute the concatenated Jacobian matrix (derivatives with
%               respect to r).
% INPUT:    r   Receiver's state (R^6).
%           x   Trajectory (R^(3*n)).
%           numpoints   Number of points in one trajectory.
% OUTPUT:   Gr  2n-by-6 Jacobian matrix (sparse).
% DEPENDENCIES: G_dr(...)   
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function Gr = G_dr_concat(r,x,numpoints)
Gr = sparse(2*numpoints,6);
x = reshape(x,3,numpoints);
for ii = 1:numpoints
    Gr(2*ii-1:2*ii,:) = G_dr(r,x(:,ii));
end