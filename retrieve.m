% ===================================
% FUNCTION phi = retrieve(log_phi)
% -----------
% DESCRIPTION:  Convert log_phi to phi in order to obtain multinomial
%               parameters.
% INPUT:    log_phi   M-by-1 vector with log of multinomial parameters.
% OUTPUT:   phi       M-by-1 vector with multinomial parameters.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function phi = retrieve(log_phi)
phi = exp(log_phi);
phi = phi./sum(phi);
end