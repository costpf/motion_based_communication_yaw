% ===================================
% FUNCTION [] = visualize(true_r, true_x, est_r, est_x, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w)
% -----------
% DESCRIPTION: Visualize the simulation.
% INPUT:    true_r      True state of the receiver in the traj. coord.
%           true_x      True trajectory in the traj. coord.
%           est_r       Estimated state of the receiver.
%           est_x       Estimated trajectory.
%           z           Observation of the trajectory taken.
%           log_phi     Log of the multinomial parameters.
%           true_class  True trajectory class.
%           visible_flags   Visible flags for the points in the trajectory.
%           numpoints   Number of points in the trajectory.
%           true_Rot_traj2w True rotation matrix from traj. to world coord.
%           true_trans_traj2w   True translation vector from traj. to w.
% -----------
% DEPENDENCIES: skew(x), inv_skew(X), camera_param.mat, retrieve(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
%   Haruki Nishimura    12-02-16    <-  Updated to plot the multinomial
%                                       distribution.
% ===================================
function visualize(true_r, true_x, est_r, est_x, z, log_phi, true_class, visible_flags, numpoints, true_Rot_traj2w, true_trans_traj2w)
    function [true_camera_pose, body_pos, body_att, est_traj_pos, est_traj_att, true_x, est_x] = convert2world(true_r, true_x, est_r, est_x, numpoints, true_Rot_traj2w, true_trans_traj2w, R_bc, d_bc)
        % In the trajectory coordinates.
        true_t = true_r(4:6);
        true_w = true_r(1:3);
        est_t = est_r(4:6);
        est_w = est_r(1:3);
        
        % Body position and rotation in the world coordinates. (Known <- Optitrack)
        body_pos = true_Rot_traj2w*true_t + true_trans_traj2w;
        body_att = inv_skew(logm(true_Rot_traj2w*expm(skew(true_w))));
        if norm(body_att) > pi; % Avoid singularities.
            body_att = (1 - 2*pi/norm(body_att))*body_att;
        end
        
        % Estimated trajectory coordinates.
        est_traj_att = inv_skew(logm(expm(skew(body_att))*expm(skew(est_w))'));
        if norm(est_traj_att) > pi; % Avoid singularities.
            est_traj_att = (1 - 2*pi/norm(est_traj_att))*est_traj_att;
        end
        est_traj_pos = body_pos - expm(skew(est_traj_att))*est_t;
        
        for ii = 1:numpoints
             % True sender position.
            true_x(3*ii-2:3*ii) = true_Rot_traj2w*true_x(3*ii-2:3*ii) + true_trans_traj2w;
             % Estimated sender position.
            est_x(3*ii-2:3*ii) = expm(skew(est_traj_att))*est_x(3*ii-2:3*ii) + est_traj_pos;
        end
        
        % calculate camera pose in the world coordinates.(external parameters matrix with [R|t])
        true_camera_rot = expm(skew(body_att))*R_bc';
        true_camera_trans = body_pos + expm(skew(body_att))*[d_bc;0;0];
        true_camera_pose = [true_camera_rot, true_camera_trans];
    end
    

load('camera_param.mat', 'K', 'R_bc', 'd_bc');
u_max = 2*K(1,3);
v_max = 2*K(2,3);

% values are in the world coordinates.
[true_camera_pose, body_pos, body_att, est_traj_pos, est_traj_att, true_x, est_x] = convert2world(true_r, true_x, est_r, est_x, numpoints, true_Rot_traj2w, true_trans_traj2w, R_bc, d_bc);

clf(figure(2));
figure(2);
set(gcf, 'Position', [100, 100, 900, 700]);
grid on; hold on;
% draw world axes.
line([0,4],[0,0],[0,0],'Linestyle','-','Color','r');
line([0,0],[0,4],[0,0],'Linestyle','-','Color','g');
line([0,0],[0,0],[0,4],'Linestyle','-','Color','b');
xlabel('X_w'); ylabel('Y_w'); zlabel('Z_w');
% draw true trajectory axes.
e1_traj = true_Rot_traj2w*[4;0;0] + true_trans_traj2w;
e2_traj = true_Rot_traj2w*[0;4;0] + true_trans_traj2w;
e3_traj = true_Rot_traj2w*[0;0;4] + true_trans_traj2w;
line([true_trans_traj2w(1),e1_traj(1)],[true_trans_traj2w(2),e1_traj(2)],[true_trans_traj2w(3),e1_traj(3)],'Linestyle','-','Color','r');
line([true_trans_traj2w(1),e2_traj(1)],[true_trans_traj2w(2),e2_traj(2)],[true_trans_traj2w(3),e2_traj(3)],'Linestyle','-','Color','g');
line([true_trans_traj2w(1),e3_traj(1)],[true_trans_traj2w(2),e3_traj(2)],[true_trans_traj2w(3),e3_traj(3)],'Linestyle','-','Color','b');
% draw estimated trajectory axes.
e1_traj_est = expm(skew(est_traj_att))*[4;0;0] + est_traj_pos;
e2_traj_est = expm(skew(est_traj_att))*[0;4;0] + est_traj_pos;
e3_traj_est = expm(skew(est_traj_att))*[0;0;4] + est_traj_pos;
line([est_traj_pos(1),e1_traj_est(1)],[est_traj_pos(2),e1_traj_est(2)],[est_traj_pos(3),e1_traj_est(3)],'Linestyle','-.','Color','r');
line([est_traj_pos(1),e2_traj_est(1)],[est_traj_pos(2),e2_traj_est(2)],[est_traj_pos(3),e2_traj_est(3)],'Linestyle','-.','Color','g');
line([est_traj_pos(1),e3_traj_est(1)],[est_traj_pos(2),e3_traj_est(2)],[est_traj_pos(3),e3_traj_est(3)],'Linestyle','-.','Color','b');

%{
% draw camera axes.
e1_cam = true_camera_pose(:,1:3)*[2;0;0] + true_camera_pose(:,4);
e2_cam = true_camera_pose(:,1:3)*[0;2;0] + true_camera_pose(:,4);
e3_cam = true_camera_pose(:,1:3)*[0;0;2] + true_camera_pose(:,4);
line([true_camera_pose(1,4),e1_cam(1)],[true_camera_pose(2,4),e1_cam(2)],[true_camera_pose(3,4),e1_cam(3)],'Linestyle','-','Color','r');
line([true_camera_pose(1,4),e2_cam(1)],[true_camera_pose(2,4),e2_cam(2)],[true_camera_pose(3,4),e2_cam(3)],'Linestyle','-','Color','g');
line([true_camera_pose(1,4),e3_cam(1)],[true_camera_pose(2,4),e3_cam(2)],[true_camera_pose(3,4),e3_cam(3)],'Linestyle','-','Color','b');
axis equal;
%}

% draw body axes.
e1_body = expm(skew(body_att))*[2;0;0] + body_pos;
e2_body = expm(skew(body_att))*[0;2;0] + body_pos;
e3_body = expm(skew(body_att))*[0;0;2] + body_pos;
line([body_pos(1), e1_body(1)],[body_pos(2), e1_body(2)],[body_pos(3),e1_body(3)],'LineStyle','-','Color','r');
line([body_pos(1), e2_body(1)],[body_pos(2), e2_body(2)],[body_pos(3),e2_body(3)],'LineStyle','-','Color','g');
line([body_pos(1), e3_body(1)],[body_pos(2), e3_body(2)],[body_pos(3),e3_body(3)],'LineStyle','-','Color','b');


scatter3(body_pos(1),body_pos(2),body_pos(3),'r');
plotCamera('Location',true_camera_pose(:,4)','Orientation',transpose(true_camera_pose(:,1:3)),'Opacity',0, 'Size',0.4);
axis equal;
azimuth = 180/pi*(1/2*(atan2(body_pos(1), body_pos(2)) + atan2(true_trans_traj2w(1), true_trans_traj2w(2))) + 90);
alt = 25;
view(azimuth,alt); % change default view angle.
%xlim([0,20]);
%ylim([0,20]);
%zlim([0,10]);
h1 = cell(1,numpoints);
h2 = cell(1,numpoints);
for jj = 1:numpoints
    h1{jj} = scatter3(true_x(3*jj-2),true_x(3*jj-1),true_x(3*jj),'r');
    h2{jj} = scatter3(est_x(3*jj-2),est_x(3*jj-1),est_x(3*jj),'b');
    drawnow;
    pause(1/numpoints);
end
%{
pause(0.5);
for jj = 1:numpoints-1
    set(h1{jj},'Visible','off');
    set(h2{jj},'Visible','off');
end
%}

drawnow;


clf(figure(1));
figure(1);
set(gcf, 'Position', [1050, 200, 0.5*u_max, 0.5*v_max]);
hold on;
set(gca,'Ydir','reverse');
for jj = 1:numpoints
    if visible_flags(jj) == 1
        scatter(z(2*jj-1), z(2*jj), 'ro', 'linewidth',2.0);
    end
end
xlabel('u');
ylabel('v');
plot([0,u_max,u_max,0,0],[0,0,v_max,v_max,0],'m');
axis equal;
drawnow;

phi = retrieve(log_phi);
clf(figure(3));
figure(3);
set(gcf, 'Position', [1050, 550, 0.5*u_max, 0.5*v_max]);
bar(phi,'b');
hold on;
phi_true_only = zeros(length(phi),1);
phi_true_only(true_class) = phi(true_class);
bar(phi_true_only,'r');
hold off;
set(gca,'YScale','log')
set(gca,'ygrid','on')
xlabel('Trajectory class');
ylabel('Probability');
ylim([1e-300, 1]);
drawnow;
end