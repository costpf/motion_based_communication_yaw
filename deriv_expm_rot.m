% ===================================
% FUNCTION [dR_dw1, dR_dw2, dR_dw3] = deriv_expm_rot(w)
% -----------
% DESCRIPTION: Compute derivatives of a rotation matrix in its
%              exponential coordinates.
% INPUT:    w   Exponential map in so(3).
% OUTPUT:   dR_dw1  Derivatives w.r.t. the 1st element in w.
%           dR_dw2  Derivatives w.r.t. the 2nd element in w.
%           dR_dw3  Derivatives w.r.t. the 2nd element in w.
% -----------
% DEPENDENCIES: skew(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function [dR_dw1, dR_dw2, dR_dw3] = deriv_expm_rot(w)
R = expm(skew(w));

dR_dw1 = (w(1)*skew(w) + skew(cross(w,(eye(3) - R)*[1;0;0])))/norm(w)^2*R;
dR_dw2 = (w(2)*skew(w) + skew(cross(w,(eye(3) - R)*[0;1;0])))/norm(w)^2*R;
dR_dw3 = (w(3)*skew(w) + skew(cross(w,(eye(3) - R)*[0;0;1])))/norm(w)^2*R;
end