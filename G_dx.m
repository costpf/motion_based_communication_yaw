% ===================================
% FUNCTION Gx = G_dx(r,x)
% -----------
% DESCRIPTION:  Compute Jacobian matrix (derivatives of one point in the observation z with
%               respect to corresponding point in the trajectory x).
% INPUT:    r   Receiver's state (R^6).
%           x   Trajectory (R^(3*n)).
% OUTPUT:   Gx  2-by-3 Jacobian matrix.
% DEPENDENCIES: proj_mat(r)   
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function Gx = G_dx(r,x)
P = proj_mat(r);
proj = P*[x;1];
% Prevent Singularity;
if abs(proj(3)) < 5e-3
    proj(3) = proj(3) + sign(proj(3))*5e-3;
end
u = proj(1)/proj(3);
v = proj(2)/proj(3);
Gx = [P(1,1) - P(3,1)*u, P(1,2) - P(3,2)*u, P(1,3) - P(3,3)*u;
      P(2,1) - P(3,1)*v, P(2,2) - P(3,2)*v, P(2,3) - P(3,3)*v]./proj(3);
end