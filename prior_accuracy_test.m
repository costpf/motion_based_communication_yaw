numpoints = 20;
numtrials = 50;
Codebook = code_define_ellipses(numpoints);
true_class = 3;

prior_accuracy = 0;

for ii = 1:numtrials;
    % Define trajectory coordinates (in world coordinates).
    w_traj2w = [0;0;pi/6];
    true_Rot_traj2w = expm(skew(w_traj2w));
    true_trans_traj2w = [10;5;5];
    
    % Initial configuration (in trajectory coordinates).
    true_r = [inv_skew(logm(true_Rot_traj2w')); true_Rot_traj2w'*[-8;0;-1.5]];
    
    % Initial trajectory generation.
    true_x = Codebook{true_class} + transpose(mvnrnd(sparse(3*numpoints,1),full(S)));
    
    % Take initial observation and Initialize belief.
    [z,visible_flag,visible_flags] = observe_actual(true_r, true_x);
    if visible_flag == 0
        disp('Sender Not Fully Observed.');
    end
    [mu, Sigma, log_phi] = prior_rob(z, Codebook);
    [~,likely] = max(log_phi);
    if likely == true_class
        prior_accuracy  = prior_accuracy + 1/numtrials;
    end
end

