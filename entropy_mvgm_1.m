function H1 = entropy_mvgm_1(phi,mu,Sigma)
mu_arr = cat(2,mu{:})';
Sigma_arr = cat(3,Sigma{:});
gmobj = gmdistribution(mu_arr,Sigma_arr,phi');
meanpd = pdf(gmobj, mu_arr);
log_gm_mean = zeros(length(meanpd),1);
for ii = 1:length(meanpd)
    if meanpd(ii) == 0
        log_gm_mean(ii) = -746; % exp(-745) ~= 0 and exp(-746) = 0;
    else
        log_gm_mean(ii) = log(meanpd(ii));
    end
end
H1 = -dot(phi, log_gm_mean);
end